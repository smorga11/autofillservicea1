# How the Android Autofill Framework supports (or doesn't support) privacy

**(Section 1 - two chips)**

The Android Autofill Framework was introduced with Android Oreo (8.0) in 2017, giving developers the tools to implement autofill features in password managers. This autofill feature is referred to as an _autofill service_. As the name implies, this service allows users to complete forms quickly and conveniently by automatically filling the form fields with the relevant user's information.

![Autofill example](img/autofillPicture.png)

This is not a new concept of course. Autofill has been around for almost 20 years, starting with browser-based managers (e.g. Safari, Internet Explorer) and third-party software (e.g. KeePass), and later, with smartphone-based managers. Oreo introduced settings to give the user a choice of which autofill service they'd like to use, allowing third-party password managers (e.g. Dashlane, 1Password, or LastPass) to incorporate autofill into their Android apps. However, as a service that relies on directly handling incredibly sensitive information, there are a number of risks associated with the implementation of autofill and password managers. We, as developers, have an obligation to mitigate these risks when building and maintaining our own autofill services.

### First of all, how does it work?
\
\
![Autofill framework diagram](img/autofillframework.png)

There are three important parties in the Android Autofill Framework:
1. The user application
2. The Android system
3. The autofill service

The framework, as explained in the [documentation](https://developer.android.com/reference/android/service/autofill/AutofillService), follows a straightforward procedure. The flow begins when a user focuses an editable `View`, for example an `EditText` element. A `ViewStructure` of all the views in the current activity is then generated, and the Android System binds to the autofill service. This view structure is sent to the service in the function `onFillRequest`, where the bulk of processing will be done. The service parses through the view structure, analyzing each individual view element for attributes that indicate its autofill status. These attributes are `autofillHints` and `importantForAutofill`, which are set by the developers of the app, and are critical for the service to be able to identify what datasets to retrieve and where to inject that data. It does this by creating a `Dataset`, which holds the values of the data to be injected in `AutofillValue`, and the location of the field to be injected in `AutofillId`. The service then packs this data up and sends it back to the Android system as a `Response` object, after which the system unbinds from the service and displays the UI prompt underneath the relevant fields. Finally, the user picks an option, and the prepared data is injected into the views. Simple as that!

### Issues of responsibility

An autofill service must be robust and versatile. Developers of a service are responsible for the bulk of the processing; while Android provides some guidance through documentation as well as an overwhelming, yet comprehensive [sample](https://github.com/android/input-samples/tree/main/AutofillFramework), the implementation is almost entirely left up to these developers. As a consequence, the majority of vulnerabilities found in autofill services on Android can be explained by poor implementation and oversight by the services themselves. But third-party developers don't deserve all the blame here. As author Mark Murphy [explains in detail in a white paper from 2017](https://github.com/commonsguy/AutofillFollies/blob/master/WHITE_PAPER.md), Google and the documentation do little to advise autofill service developers on how to protect their users from malicious activities; instead they "work closely with major autofill providers", essentially leaving smaller providers in the dust. 

Considering the limited amount of public information on the subject, it is difficult to tell if Google's position has changed significantly from the stance they had in 2017. Incremental changes have been made since then, according to Murphy. Glancing at the documentation today, it is quite informative on _what_ can be done to reduce risk, with a little less emphasis on the _how_. However, before we can dive further into critique of the current state of Android autofill, we need to know what vulnerabilities actually lie within the framework.

### What are the flaws?

#### Hidden fields and widgets

Out of the box, the autofill framework will fill in widgets that the user can't see. For example, the user will use autofill to fill in the visible username and password fields, without realizing that the autofill service is unintentionally injecting other data (such as their address or credit card information) into hidden fields, effectively leaking their information to the website. There are a few ways in which a field or widget can be hidden (Aonzo, Merlo, Tavella, & Fratantonio, 2018):

1. Transparency: alpha value, from 0 (transparent) to 1 (opaque)
2. Small size: setting the dimensions of the field to be virtually invisible
3. Same color background and foreground
4. Invisible flag (`View.INVISIBLE`)

Murphy also adds a couple more:

5. Negative margins on the widget, causing it to display off-screen
6. The widget hides behind another opaque widget

Aonzo et al. (2018) tested some popular password managers on their vulnerability to this exploit, and found that current [popular] password managers autofill hidden fields. Unfortunately, they don't specify which leading password managers were vulnerable, nor which types of hidden fields worked, save for #3 (the framework overlays fields with yellow, so it is not possible). Suffice to say, the framework does not directly control for this case; it is up to the developers to implement a solution.

#### Master password/PIN brute forcing

Password managers often give users on smartphones the option to unlock their credentials using a 4-digit PIN for convenience. Carr and Siamak (2020) discovered that the Roboform and Dashlane apps on Android both failed to correctly implement a counter for incorrect PIN entries. They found it to be possible to try two PINs, clear the app from memory, open the app and try again. With the combined weakness of a 4-digit PIN, and with the relative ease of the process, they estimated that an attacker could theoretically brute force the PIN in 1.5-2.5 hours, and gain access to the majority of the user's account. Although somewhat trivial, it is important for service developers to not overlook this vulnerability and make sure incorrect tries are stored persistently.

#### Phishing: Package and website verification

Autofill services can provide app-specific data (e.g., login credentials) by identifying the application that the user is interacting with. Theoretically, if a malicious app can fool the service into thinking it is a legitimate app, the service could inadvertently provide sensitive data (this is called _phishing_). While the Android Autofill Framework has methods to verify the app and/or website using signing certificates and [digital asset links](https://developers.google.com/digital-asset-links/v1/getting-started), Aonzo et al. (2018) discovered that three of the top five password managers at the time used simple, exploitable heuristics to verify an app's association with a web backend, and were thus vulnerable to phishing attacks. However, this may be due to a slow adoption of the new autofill framework than anything else, as the study was done very soon after the release of the framework. Still, it highlights the disheartening fact that Android security is very inconsistent and relies heavily on the solutions of its third-party developers.

## What can we do to mitigate these flaws?

**(Section 3 - two chips)**

#### Hidden fields

Google's primary response to hidden fields is by recommending developers implement [_data partioning_](https://developer.android.com/reference/android/service/autofill/AutofillService#data-partitioning). Simply put, services can organize the users' data and the views on a screen into groups they call "partitions", for example, login credentials, address, and payment information. The service then only provides the partition that is related to the user's focused view. This way, only a portion of the data is provided, as opposed to data associated with each individual autofillable view on the screen. As an example of this in action, consider a form that asks for username, password, and address information. The user would first focus the username field, autofill both the username and password fields, and afterwards, focus and autofill the address fields separately. This, by design, mitigates the risks of hidden fields significantly.

However, the effectiveness of this depends on the extent to which the data is partitioned. Consider another situation where a service partitions the user's username and password together. If a malicious form were to ask for the username, and hide the password field, the user would still unknowingly give up that sensitive information. Therefore, to increase security, it might be of interest for service developers to separate less sensitive data (username) from its corresponding sensitive data (password). Additional partitioning such as this comes with a tradeoff in convenience; the user would end up focusing more fields, and save less time, begging the question: how much partitioning is worth the reduced risk?

I provide the following code as an example of how to implement data partioning. The base code can be found in the [documentation](https://developer.android.com/guide/topics/text/autofill-services#fill). These functions traverse the view structure, and identifies the focused view and its corresponding `autofillHints`:
```java
public void traverseStructure(AssistStructure structure) {
    int nodes = structure.getWindowNodeCount();

    for (int i = 0; i < nodes; i++) {
        WindowNode windowNode = structure.getWindowNodeAt(i);
        ViewNode viewNode = windowNode.getRootViewNode();
        traverseNodeForFocusedNode(viewNode);
    }
}

public void traverseNodeForFocusedNode(ViewNode viewNode) {
    if(viewNode.getAutofillHints() != null && viewNode.getAutofillHints().length > 0) {
        if (!viewNode.isFocused())) { // If the view node is not focused, we move on.
            continue;
        } else { // Otherwise, we check the autofill hint, and retrieve data based on that.
            String[] hints = viewNode.getAutofillHints();
            for (int i = 0; i<hints.length; i++) {
                if (hints[i].equals(View.AUTOFILL_HINT_USERNAME)) {
                    usernameAutofillId = viewNode.getAutofillId();
                }
                // You would have to implement own heuristics to check for other types of autofill hints as well.
            }

        }
    }

    for(int i = 0; i < viewNode.getChildCount(); i++) {
        ViewNode childNode = viewNode.getChildAt(i);
        traverseNodeForFocusedNode(childNode);
    }
}
```

Note that there are plenty of [autofill hint constants](https://developer.android.com/reference/androidx/autofill/HintConstants) that you can check for, assuming the client app uses these constants (which should be assumed), so service developers can handle all types of fields. Google recommends that developers who want their app to be compatible with autofill should use these hint constants, however they do not require  it, which is somewhat questionable.

Once the `autofillId` from the focused view is retrieved, the service can decide how to build its `FillResponse` object. For example, we can use the following code from the [data partioning documentation](https://developer.android.com/reference/android/service/autofill/AutofillService#data-partitioning) in the case that the focused view is a username field:

```java
 new FillResponse.Builder() // The first fill response, called when the user focuses a username or password field
     .addDataset(new Dataset.Builder() // partition 1 (credentials)
         .setValue(id1, AutofillValue.forText("homer"), createPresentation("homer"))
         .setValue(id2, AutofillValue.forText("D'OH!"), createPresentation("password for homer"))
         .build())
     .addDataset(new Dataset.Builder() // partition 1 (credentials)
         .setValue(id1, AutofillValue.forText("flanders"), createPresentation("flanders"))
         .setValue(id2, AutofillValue.forText("OkelyDokelyDo"), createPresentation("password for flanders"))
         .build())
     .setSaveInfo(new SaveInfo.Builder(SaveInfo.SAVE_DATA_TYPE_PASSWORD,
         new AutofillId[] { id1, id2 })
             .build())
     .build();
```

There are some other ways, albeit somewhat crude, in which service developers could control for hidden fields. Each view has various properties whose values we can evaluate. The autofill documentation does not make it very clear how much information is available to the autofill service for each view (the Android system strips away some information before sending it to the service), but with some quick testing, I can confirm that the service has access to `View.getAlpha`, `View.getHeight`, `View.getWidth`, and `View.getVisibility`. Services could then do easy, quick checks for "obvious" hidden fields before considering them for autofill.

Here's an example, using the basic code for traversing a view structure and its view nodes from the [documentation](https://developer.android.com/guide/topics/text/autofill-services#fill) again:
```java
public void traverseStructure(AssistStructure structure) {
    int nodes = structure.getWindowNodeCount();

    for (int i = 0; i < nodes; i++) {
        WindowNode windowNode = structure.getWindowNodeAt(i);
        ViewNode viewNode = windowNode.getRootViewNode();
        traverseNode(viewNode);
    }
}

public void traverseNode(ViewNode viewNode) {
    if(viewNode.getAutofillHints() != null && viewNode.getAutofillHints().length > 0) {
        if (viewNode.getAlpha < 0.05 || viewNode.getHeight < 5 || viewNode.getWidth < 5 || viewNode.getVisibility != 0) {
            // In this case, the viewNode may be virtually invisible (depends on our definition of invisible)
            // Should then warn the user of the potentially malicious form.
        }
    }

    for(int i = 0; i < viewNode.getChildCount(); i++) {
        ViewNode childNode = viewNode.getChildAt(i);
        traverseNode(childNode);
    }
}
```

It is unclear why Google does not suggest using the `View`'s properties to check for virtually invisible fields, and why they do not provide clear information on what the service can access. 
 
#### Authentication

An easy way to get an extra layer of security on autofill data is to lock it behind an authentication prompt. While not a solution for most malicious form issues, it is good for situations in which the user's device might fall in the wrong hands. This is particularly useful for protecting credit card information behind a CVC, or protecting any login credentials, really.

User authentication is simple to implement in the autofill service. Instead of building a dataset right away upon calling `onFillRequest`, we instead create a `IntentSender` object that sends us to a new activity, where we can handle the authentication through a master password or biometrics on a separate screen. Once we successfully authenticate ourselves in that new activity, we are sent back to the login form, with the option to login with our unlocked credentials.

The following code is pulled straight from the [documentation on user authentication in autofill](https://developer.android.com/guide/topics/text/autofill-services#auth):

Before authentication, in our `onFillRequest` function of the service:
```java
RemoteViews authPresentation = new RemoteViews(getPackageName(), android.R.layout.simple_list_item_1);
authPresentation.setTextViewText(android.R.id.text1, "requires authentication");
Intent authIntent = new Intent(this, AuthActivity.class); // AuthActivity is where we go to authenticate.

// Send any additional data required to complete the request.
authIntent.putExtra(MY_EXTRA_DATASET_NAME, "my_dataset");
IntentSender intentSender = PendingIntent.getActivity(
                this,
                1001,
                authIntent,
                PendingIntent.FLAG_CANCEL_CURRENT
        ).getIntentSender();

// Build a FillResponse object that requires authentication.
FillResponse fillResponse = new FillResponse.Builder()
        .setAuthentication(autofillIds, intentSender, authPresentation)
        .build(); // can add more objects to the builder here as well, like setSaveInfo!
```

In our `AuthActivity` class, the service developer should implement their preferred authentication method (master password or biometrics). The developer should then call the following code upon successful authentication: 

```java
Intent intent = getIntent();

// The data sent by the service and the structure are included in the intent.
String datasetName = intent.getStringExtra(MY_EXTRA_DATASET_NAME);
AssistStructure structure = intent.getParcelableExtra(EXTRA_ASSIST_STRUCTURE);
ParsedStructure parsedStructure = parseStructure(structure);
UserData userData = fetchUserData(parsedStructure);

// Build the presentation of the datasets.
RemoteViews usernamePresentation = new RemoteViews(getPackageName(), android.R.layout.simple_list_item_1);
usernamePresentation.setTextViewText(android.R.id.text1, "my_username");
RemoteViews passwordPresentation = new RemoteViews(getPackageName(), android.R.layout.simple_list_item_1);
passwordPresentation.setTextViewText(android.R.id.text1, "Password for my_username");

// Add the dataset to the response.
FillResponse fillResponse = new FillResponse.Builder()
        .addDataset(new Dataset.Builder()
                .setValue(parsedStructure.usernameId,
                        AutofillValue.forText(userData.username), usernamePresentation)
                .setValue(parsedStructure.passwordId,
                        AutofillValue.forText(userData.password), passwordPresentation)
                .build())
        .build();

Intent replyIntent = new Intent();

// Send the data back to the service.
replyIntent.putExtra(MY_EXTRA_DATASET_NAME, datasetName);
replyIntent.putExtra(EXTRA_AUTHENTICATION_RESULT, fillResponse);

setResult(RESULT_OK, replyIntent);
```

The [autofill authentication documentation](https://developer.android.com/guide/topics/text/autofill-services#auth) also has details on how to implement authentication for credit card data using CVC, if you want to learn more.

#### Package verification and web security

From the [package verification documentation](https://developer.android.com/reference/android/service/autofill/AutofillService#package-verification):
> When autofilling app-specific data (like username and password), the service must verify the authenticity of the request by obtaining all signing certificates of the app being autofilled, and only fulfilling the request when they match the values that were obtained when the data was first saved — such verification is necessary to avoid phishing attempts by apps that were sideloaded in the device with the same package name of another app.

In order to implement proper package verification, the service developers need to store the package signing certificates alongside login credentials when a user logs in for the first time in an application. Every subsequent login in that application will then require a quick check by the service to see if the packages are the same before providing data.

The documentation on package verification also provides a good example of how to easily retrieve the certificates:

```java
 private String getCertificatesHash(String packageName) throws Exception {
   PackageManager pm = mContext.getPackageManager();
   PackageInfo info = pm.getPackageInfo(packageName, PackageManager.GET_SIGNATURES);
   ArrayList hashes = new ArrayList<>(info.signatures.length);
   for (Signature sig : info.signatures) {
     byte[] cert = sig.toByteArray();
     MessageDigest md = MessageDigest.getInstance("SHA-256");
     md.update(cert);
     hashes.add(toHexString(md.digest()));
   }
   Collections.sort(hashes);
   StringBuilder hash = new StringBuilder();
   for (int i = 0; i < hashes.size(); i++) {
     hash.append(hashes.get(i));
   }
   return hash.toString();
 }
```

A more difficult problem is when a form in an app represents a web page. The service developer needs to get the source of the web form (using `AssistStructure.ViewNode.getWebDomain()`), find its canonical domain, use digital asset links (DALs) to get the package name and certificate associated with that canonical domain, and match the certificate to the currently running application using the same `getCertificatesHash()` function as above.

We'll implement this by checking if the root node is a `WebView`, as suggested in the [web security documentation](https://developer.android.com/reference/android/service/autofill/AutofillService#web-security). If it is, we'll get the canonical web domain (`getCanonicalDomain()`) using code provided in the same docs.

```java
public void traverseStructure(AssistStructure structure) {
    int nodes = structure.getWindowNodeCount();

    for (int i = 0; i < nodes; i++) {
        WindowNode windowNode = structure.getWindowNodeAt(i);
        ViewNode viewNode = windowNode.getRootViewNode();
        if (viewNode.getClassName().equals("WebView")) { // Identify that the autofill request is coming from a web page
            String source = ViewNode.getWebDomain(); // get the web domain
            String canonicalSource = getCanonicalDomain(source); // get the canonical web domain
            Boolean verify = verifyDAL(canonicalSource); // verify the DAL (more below)
            traverseWebNode(viewNode);
        }
    }
}

public void traverseWebNode(ViewNode viewNode) {
    if(viewNode.getAutofillHints() != null && viewNode.getAutofillHints().length > 0) {
        // Do standard autofill parsing.
    }

    for(int i = 0; i < viewNode.getChildCount(); i++) {
        ViewNode childNode = viewNode.getChildAt(i);
        traverseWebNode(childNode);
    }
}

private static String getCanonicalDomain(String domain) {
   InternetDomainName idn = InternetDomainName.from(domain);
   while (idn != null && !idn.isTopPrivateDomain()) {
     idn = idn.parent();
   }
   return idn == null ? null : idn.toString();
}
 ```

Now we need to verify the site association using DALs. To do this, we'll have to make an HTTP GET request for a specific JSON file, which every website must have to be associated with an Android app. Each JSON file for a canonical domain name can be found at http://canonical-domain-name.com/.well-known/assetlinks.json. The JSON contains a property for the Android package name and certificate fingerprints. By retrieving these values, we can use `getCertificatesHash` from above to match the values returned by the currently running Android package. See [here](https://developer.android.com/training/app-links/verify-site-associations#publish-json) for more information on the JSON file, and [here](https://developers.google.com/digital-asset-links/v1/consuming#validating_manually) for how to retrieve it.

 ```java
private static Boolean verifyDAL(String canonicalDomain) {
    // implement HTTP GET request for the json
    // Parse the JSON to retrieve the package name and certificate fingerprints
    // Check the package name and certificate fingerprints for a match against the currently running Android application
}
```

#### PIN brute forcing, and master password policies

There are different ways to store the number of incorrect tries in persistent memory, but the most appropriate method would be to use the `SharedPreferences` [API](https://developer.android.com/training/data-storage/shared-preferences). Read the documentation for a simple look into how to write to the file. Keeping track of the number of incorrect tries, and then storing them into a `SharedPreferences` file, is relatively trivial, and strays from the topic of this article. But it still important to implement, along with strong master password policies. [Google](https://support.google.com/accounts/answer/32040?hl=en) has good rules to follow on password requirements.


## Parting words

Building an autofill service is no easy feat. As we saw in some research papers, even the most popular password managers fail to get it right, at least the first time. The best advice service developers can take is to study Google's security recommendations very closely. While the documentation may leave some things to be desired, it still does its job well, and points developers in the right direction. Implementation will always be difficult to get right, which is why it is so important that service developers take every step of the development process very seriously. While it is strongly suggested that the best advice for developers is to simply not write an autofill service, I believe that it is more accessible today than it was when it was first released.

## References and links:

**Autofill documentation**:

[Building an autofill service](https://developer.android.com/guide/topics/text/autofill-services)

[Optimize your app for autofill](https://developer.android.com/guide/topics/text/autofill-optimize)

[AutofillService](https://developer.android.com/reference/android/service/autofill/AutofillService)

[Autofill sample in Java](https://github.com/android/input-samples/tree/main/AutofillFramework)

[Autofill hint constants](https://developer.android.com/reference/androidx/autofill/HintConstants)

[Digital asset links](https://developers.google.com/digital-asset-links/v1/getting-started)

[DAL JSON file info]([https://developer.android.com/training/app-links/verify-site-associations#publish-json)

[DAL JSON file processing](https://developers.google.com/digital-asset-links/v1/consuming#validating_manually)

[Shared preferences](https://developer.android.com/training/data-storage/shared-preferences)

[Google password recommendations](https://support.google.com/accounts/answer/32040?hl=en)

**Papers**:

[Autofill Services and Security, White Paper by Mark Murphy](https://github.com/commonsguy/AutofillFollies/blob/master/WHITE_PAPER.md)

Aonzo, S., Merlo, A., Tavella, G., & Fratantonio, Y. (2018, October). Phishing attacks on modern android. In Proceedings of the 2018 ACM SIGSAC Conference on Computer and Communications Security (pp. 1788-1801). [Link to pdf](http://www.s3.eurecom.fr/projects/modern-android-phishing/ccs18-modern-phishing.pdf)

Carr, M., & Shahandashti, S. F. (2020, September). Revisiting security vulnerabilities in commercial password managers. In IFIP International Conference on ICT Systems Security and Privacy Protection (pp. 265-279). Springer, Cham. [Link to pdf](https://arxiv.org/pdf/2003.01985.pdf)
