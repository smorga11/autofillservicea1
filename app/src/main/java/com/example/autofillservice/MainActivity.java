package com.example.autofillservice;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.transition.Transition;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.autofill.AutofillManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    // Using tutorial for login and register screens https://www.youtube.com/watch?v=1WPAXHhG6u0

    DatabaseHelper db;
    private EditText etUsername, etPassword;
    private Button btnLogin, btnSettings, btnClear;
    private AutofillManager afm;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        afm = getBaseContext().getSystemService(AutofillManager.class);
        db = new DatabaseHelper(this, "Login.db");
        //Log.i("db", this.getDatabasePath("Login.db").getPath());

        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPass);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnSettings = (Button) findViewById(R.id.btnSettings);
        btnClear = (Button) findViewById(R.id.btnClear);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { login(); }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { resetFields(); }
        });

        btnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { openSettings(); }
        });



    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void resetFields() {
        etUsername.setText("");
        etPassword.setText("");
        afm.cancel();
    }

    private void openSettings() {
        Intent intent = SettingsActivity.getStartActivityIntent(MainActivity.this);
        startActivity(intent);
    }

    // Simple, dummy login authentication
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void login() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();

        if(handleCredentials(username, password)) {
            //Toast.makeText(this, "Login successful.", Toast.LENGTH_SHORT).show();
            afm.commit();
            Intent intent = LoginSuccessfulActivity.getStartActivityIntent(MainActivity.this);
            startActivity(intent);
            resetFields();
        } else {
            // Do nothing
        }
    }

    private boolean handleCredentials(String username, String password) {
        //return username!=null && password!=null;

        // Check that both fields aren't null, or equal to each other
        if (username.equals("") || password.equals("")) {
            Toast.makeText(this, "Could not log in - fields empty.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (username.equalsIgnoreCase(password)) {
            Toast.makeText(this, "Could not log in - username and password cannot be identical.", Toast.LENGTH_SHORT).show();
            return false;
        }

        // If username is available (unused):
        if (db.usernameAvailable(username)) {
            Boolean ins = db.insert(username, password);
            Log.i("test", "insert value = "+ins);
            if (ins) {
                Toast.makeText(this, "New account successfully created.", Toast.LENGTH_SHORT).show();
                return true;
            } else {
                Toast.makeText(this, "Error in inserting into database", Toast.LENGTH_SHORT).show();
                return false;
            }
        } else { // If username is unavailable
            Boolean check = db.checkUsernamePass(username, password);
            if (check) { // if it has a match, we can login
                Toast.makeText(this, "Username and password correctly matched", Toast.LENGTH_SHORT).show();
                return true;
            } else { // Not a match
                Toast.makeText(this, "Password incorrect.", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
    }

}