package com.example.autofillservice;

import android.app.PendingIntent;
import android.app.assist.AssistStructure;
import android.content.Intent;
import android.content.IntentSender;
import android.database.CursorIndexOutOfBoundsException;
import android.os.Build;
import android.os.CancellationSignal;
import android.provider.ContactsContract;
import android.service.autofill.AutofillService;
import android.service.autofill.Dataset;
import android.service.autofill.FillCallback;
import android.service.autofill.FillContext;
import android.service.autofill.FillRequest;
import android.service.autofill.FillResponse;
import android.service.autofill.SaveCallback;
import android.service.autofill.SaveInfo;
import android.service.autofill.SaveRequest;
import android.util.Log;
import android.view.autofill.AutofillId;
import android.view.autofill.AutofillManager;
import android.view.autofill.AutofillValue;
import android.widget.RemoteViews;
import android.widget.Toast;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import java.util.List;

@RequiresApi(api = Build.VERSION_CODES.O)
public class MyAutofillService extends AutofillService {

    // https://developer.android.com/guide/topics/text/autofill-services

    private DatabaseHelper afdb;

    @Override
    public void onCreate() {
        super.onCreate();
        afdb = new DatabaseHelper(this, "Autofill.db");
    }

    @Override
    public void onFillRequest(@NonNull FillRequest request, @NonNull CancellationSignal cancellationSignal, @NonNull FillCallback callback) {
        //Toast.makeText(this, "onFillRequest called.", Toast.LENGTH_SHORT).show();

        // Get the structure from the request
        List<FillContext> context = request.getFillContexts();
        AssistStructure structure = context.get(context.size() - 1).getStructure();

        // Traverse the structure for nodes to fill out
        ParsedStructure parsedStructure = new ParsedStructure();
        parsedStructure.parseStructure(structure, false);
        // Doesn't control for cases where we find null autofillIds

        if (parsedStructure.usernameId==null || parsedStructure.passwordId==null) {// || parsedStructure.username==null || parsedStructure.password==null) {
            callback.onFailure("Null pointers in parsed structure");
        } else {
            AutofillId[] autofillIds = {parsedStructure.usernameId, parsedStructure.passwordId};

            RemoteViews authPresentation = new RemoteViews(getPackageName(), android.R.layout.simple_list_item_1);
            authPresentation.setTextViewText(android.R.id.text1, "Unlock");
            Intent authIntent = new Intent(this, AuthActivity.class);

            //authIntent.putExtra("extra_name", "my_dataset");

            IntentSender intentSender = PendingIntent.getActivity(
                    this,
                    1001,
                    authIntent,
                    PendingIntent.FLAG_CANCEL_CURRENT
            ).getIntentSender();

            FillResponse fillResponse = new FillResponse.Builder()
                    .setAuthentication(autofillIds, intentSender, authPresentation)
                    .setSaveInfo(new SaveInfo.Builder(
                            SaveInfo.SAVE_DATA_TYPE_USERNAME | SaveInfo.SAVE_DATA_TYPE_PASSWORD,
                            new AutofillId[] {parsedStructure.usernameId, parsedStructure.passwordId})
                            .build())
                    .build();

            // pass the response
            callback.onSuccess(fillResponse);
        }
    }

    @Override
    public void onSaveRequest(@NonNull SaveRequest request, @NonNull SaveCallback callback) {
        // Get the structure from the request

        List<FillContext> context = request.getFillContexts();
        AssistStructure structure = context.get(context.size() - 1).getStructure();

        ParsedStructure parsedStructure = new ParsedStructure();
        parsedStructure.parseStructure(structure, true);

        if (parsedStructure==null || parsedStructure.usernameId==null || parsedStructure.passwordId==null || parsedStructure.username==null || parsedStructure.password==null) {
            callback.onFailure("Null pointers in parsed structure");
        } else {
            // No errors
            // Persist the data (THEORETICALLY)

            Boolean ins = afdb.insert(parsedStructure.username, parsedStructure.password);

            if (ins) {
                Toast.makeText(this, "Added to autofill db", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Failed to add to autofill db", Toast.LENGTH_SHORT).show();
            }
            callback.onSuccess();
        }
    }

}
