package com.example.autofillservice;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.assist.AssistStructure;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.service.autofill.Dataset;
import android.service.autofill.FillResponse;
import android.service.autofill.SaveInfo;
import android.view.View;
import android.view.autofill.AutofillId;
import android.view.autofill.AutofillManager;
import android.view.autofill.AutofillValue;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RemoteViews;
import android.widget.Toast;

public class AuthActivity extends AppCompatActivity {

    private Button btnAuth;
    private EditText etMaster;
    private String master;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        etMaster = (EditText) findViewById(R.id.etMaster);
        btnAuth = (Button) findViewById(R.id.btnAuthenticate);
        master = "1234";

        btnAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { Authenticate(); }
        });


    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void Authenticate() {
        
        // Check if inputted password is correct
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        String pass = etMaster.getText().toString();

        if(pass.equals(master)) {
            accessGranted();
        } else {
            //do nothing for now
            Toast.makeText(this, "Wrong master password, try again", Toast.LENGTH_SHORT).show();
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void accessGranted() {
        Intent intent = getIntent();

        AssistStructure structure = intent.getParcelableExtra(AutofillManager.EXTRA_ASSIST_STRUCTURE);
        ParsedStructure parsedStructure = new ParsedStructure();
        parsedStructure.parseStructure(structure, false);
        UserData userData = fetchUserData(parsedStructure);

        RemoteViews usernamePresentation = new RemoteViews(getPackageName(), android.R.layout.simple_list_item_1);
        RemoteViews passwordPresentation = new RemoteViews(getPackageName(), android.R.layout.simple_list_item_1);

        usernamePresentation.setTextViewText(android.R.id.text1, userData.username);
        passwordPresentation.setTextViewText(android.R.id.text1, userData.username);

        FillResponse fillResponse = new FillResponse.Builder()
                .addDataset(new Dataset.Builder()
                        .setValue(parsedStructure.usernameId,
                                AutofillValue.forText(userData.username), usernamePresentation)
                        .setValue(parsedStructure.passwordId,
                                AutofillValue.forText(userData.password), passwordPresentation)
                        .build())
                .setSaveInfo(new SaveInfo.Builder(
                        SaveInfo.SAVE_DATA_TYPE_USERNAME | SaveInfo.SAVE_DATA_TYPE_PASSWORD,
                        new AutofillId[] {parsedStructure.usernameId, parsedStructure.passwordId})
                        .build())
                .build();

        Intent replyIntent = new Intent();

        replyIntent.putExtra(AutofillManager.EXTRA_AUTHENTICATION_RESULT, fillResponse);


        setResult(RESULT_OK, replyIntent);

        finish();
    }

    public UserData fetchUserData(ParsedStructure parsedStructure) {
        UserData userData = new UserData();
        userData.username = "Username-1"; //afdb.getUsername(); //just grabs the most recently added username
        userData.password = "Password-1"; //afdb.getPassword();
        return userData;
    }

    class UserData {
        String username;
        String password;
    }
}

