package com.example.autofillservice;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {


    public DatabaseHelper(@Nullable Context context, String fileName) {
        super(context, fileName, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("Create table user(Username text primary key, Password text)");
        Log.i("test", "Created table");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists user");
        Log.i("test", "upgrade table");
    }

    // This inserts the username and password into the autofill database
    public boolean insert(String username, String password) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("Username", username);
        contentValues.put("Password", password);
        long ins = db.insert("user",null,contentValues);
        Log.i("test", "inserting username password");
        db.close();
        if(ins==-1) return false;
        else return true;
    }

    // Returns true if username is available, false otherwise.
    public Boolean usernameAvailable(String username) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select * from user where Username=?", new String[]{username});
        Log.i("test", "checking username availability");
        int c = cursor.getCount();
        cursor.close();
        db.close();
        return c <= 0;
    }

    public Boolean checkUsernamePass(String username, String password) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select * from user  where Username=? and Password=?", new String[]{username, password});
        int c = cursor.getCount();
        cursor.close();
        db.close();
        return c == 1;
    }

    public String getUsername() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select * from user", null);
        String u = null;
        try {
             u = cursor.getString(0);
        } catch (CursorIndexOutOfBoundsException e) {
            Log.w("db user", "caught CursorIndexOutOfBoundsException");
        }
        cursor.close();
        db.close();
        return u;
    }

    public String getPassword() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select * from user", null);
        String p = null;
        try {
            p = cursor.getString(1);
        } catch (CursorIndexOutOfBoundsException e) {
            Log.w("db pass", "caught CursorIndexOutOfBoundsException");
        }
        cursor.close();
        db.close();
        return p;
    }
}
