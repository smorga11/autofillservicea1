package com.example.autofillservice;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class SettingsActivity extends AppCompatActivity {

    public static Intent getStartActivityIntent(Context context) {
        return new Intent(context, SettingsActivity.class);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_afs);

        findViewById(R.id.btnSettingsReturn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.btnResetDB).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getApplicationContext().deleteDatabase("Login.db");
                getApplicationContext().deleteDatabase("Autofill.db");
                Toast.makeText(getApplicationContext(), "Deleted login and autofill databases.", Toast.LENGTH_SHORT).show();
            }
        });
    }


}
