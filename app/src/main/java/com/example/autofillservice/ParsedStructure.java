package com.example.autofillservice;

import android.app.assist.AssistStructure;
import android.os.Build;
import android.util.Log;
import android.view.autofill.AutofillId;

import androidx.annotation.RequiresApi;

public class ParsedStructure {

    public AutofillId usernameId;
    public AutofillId passwordId;
    public String username;
    public String password;


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void parseStructure(AssistStructure structure, Boolean save) {
        int nodes = structure.getWindowNodeCount();

        for(int i = 0; i<nodes; i++) {
            AssistStructure.WindowNode windowNode = structure.getWindowNodeAt(i);
            AssistStructure.ViewNode viewNode = windowNode.getRootViewNode();
            traverseNode(viewNode, save);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void traverseNode(AssistStructure.ViewNode viewNode, Boolean save) {

        if(viewNode.getAutofillHints() != null && viewNode.getAutofillHints().length > 0) {
            String[] hints = viewNode.getAutofillHints();
            for (int i = 0; i<hints.length; i++) {
                if (hints[i].contains("username")) {
                    //Log.i("view","alpha value = " + viewNode.getAlpha());
                    //Log.i("view","height value = " + viewNode.getHeight());
                    //Log.i("view","width value = " + viewNode.getWidth());
                    //Log.i("view","background color value = " + viewNode.getTextBackgroundColor());
                    //Log.i("view","text size value = " + viewNode.getTextSize());
                    //Log.i("view","visibility value = " + viewNode.getVisibility());

                    this.usernameId = viewNode.getAutofillId();
                    if(save) {
                        this.username = viewNode.getText().toString();
                    }
                }
                if (hints[i].contains("password")) {
                    this.passwordId = viewNode.getAutofillId();
                    if(save) {
                        this.password = viewNode.getText().toString();
                    }
                    
                }
            }
        } else {
            // handle for when client apps dont implement autofill hints.
        }

        for(int i = 0; i < viewNode.getChildCount(); i++) {
            AssistStructure.ViewNode childNode = viewNode.getChildAt(i);
            traverseNode(childNode, save);
        }
    }
}
